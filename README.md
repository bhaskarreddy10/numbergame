# NumberGame

## Introduction
This project implements a game where the user starts a game and a welcome message will be sent to the user with a description in it. The user chooses the number and follows the steps displayed by the system. After the last step, the system displays the number. If the displayed number and chosen number are not the same then the user might have done calculations wrong.

---

## Technology Listing:

* Node.js
* HTML
* CSS
* JQuery


---

## Prerequisites

* Visual Studio Code - https://code.visualstudio.com/download
* Node.js - https://nodejs.org/en/download/


---

## Setup

### Install Dependency 

```
npm install
```

### Start Server

```
npm server
```

### Create a package.json file

```
npm init
```

### Run Application

```
node game.ejs
```

### Access Web Browser

```
http://localhost:3000/
```
---