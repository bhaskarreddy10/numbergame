const GameState = Object.freeze({
    WELCOMING:   Symbol("welcoming"),
    START:  Symbol("start"),
    ADDED: Symbol("added"),
    DOUBLED: Symbol("doubled"),
    SECOND : Symbol("second"),    
    OK : Symbol("ok"),
    CHOSEN : Symbol("chosen"),
    NEW_GAME : Symbol("new_game"),
    TWICE : Symbol("twice"),
    HALF : Symbol("half"),
	FOUR : Symbol("four"),	
	PREDICTION : Symbol("Prediction"),
	OUTPUT : Symbol("output")
});

module.exports = class Game{
    constructor(){
        this.stateCur = GameState.WELCOMING;
    }
    
    makeAMove(sInput)
    {
        let sReply = "";
        let cNumber = 0;
        switch(this.stateCur){
            case GameState.WELCOMING:
            sReply = "Do you know one thing... If you play a game with me, I can say the number you have chosen. Shall we play? Please ipnut as 'yes' or 'no' ";
                this.stateCur = GameState.START;
                break;
            case GameState.START:
                if(sInput.toLowerCase().match("yes")){
                    sReply = "Multiply the chosen number with 5. After multiplying input as 'multiplied'";
					this.stateCur = GameState.MULTIPLIED;
                }
                else {
                    sReply = "It will be a fun try it once. please give 'yes' to give a try";
                    this.stateCur = GameState.START;
                }
                break;
            case GameState.MULTIPLIED:
                if(sInput.toLowerCase().match("multiplied")){
                    sReply = "Add 5 to your answer. After adding, input as 'added'";
                    this.stateCur = GameState.ADDED;
                }
                else {
                    sReply = "if you did not get, please look at the example. Example: if chosen number is 2, multiply 5 to number 2 and input as 'multiplied'";
                    this.stateCur = GameState.MULTIPLIED;
                }
                break;
                case GameState.ADDED:
                    if(sInput.toLowerCase().match("added")){
                        sReply = "Multiply your answer with 2 and give input as 'doubled'";
                        this.stateCur = GameState.DOUBLED;
                    }else{
                        sReply = "if you did not get, please look at the example. Example: if your answer was 10, add '5' to 10 and input as 'added'";
                        this.stateCur = GameState.ADDED;
                    }
                    break;
                case GameState.DOUBLED:
                    if(sInput.toLowerCase().match("doubled")){
                        sReply = "Add 2 to your answer and input the final Answer(only numbers)";
                        this.stateCur = GameState.SECOND;
                    }else{
                        sReply = "if you did not get, please look at the example. Example: if your answer was 15, double the number i.e., 30 and input as 'doubled'";
                        this.stateCur = GameState.DOUBLED;
                    }
                    break;
                                    
                case GameState.SECOND:
                    if (isNaN(sInput)) {
                        sReply = "Please input a number i.e., your answer";
                        this.stateCur = GameState.SECOND;
                    }else{
                        cNumber = Math.floor((sInput/10) - 1);
                        sReply = "you have chosen " + cNumber + ". If it is not the number you have chosen, then you might did calculations wrong." +
                        " Shall we play the same game one more time or shall we try a new game. Input as 'same' for same game or 'new' for new game";
                        this.stateCur = GameState.NEW_GAME;
                    }
                    break;
    
    
                    case GameState.NEW_GAME:
                    if(sInput.toLowerCase().match("same")){
                        sReply = "choose a new number..If you have chosen new number then input as 'yes' ";
                        this.stateCur = GameState.START;
                    }else if(sInput.toLowerCase().match("new")){
                        sReply = "Now we will play a prediction game. In this game, I will give you your final answer. Please input as 'ok' for proceeding";
                        this.stateCur = GameState.OK;
                    }
                    else{
                        sReply = "Please give only the specified inputs i.e., 'same' or 'new'";
                        this.stateCur = GameState.NEW_GAME;
                    }
                    break;
                    case GameState.OK:
                    if(sInput.toLowerCase().match("ok")){
                        sReply = "Choose a number preferably 1 to 99 to make calculations easy and after choosing give the input as 'chosen' ";
                        this.stateCur = GameState.CHOSEN;
                    }
                    else{
                        sReply = "Please give a valid input";
                        this.stateCur = GameState.OK;
                    }
                    break;
                    case GameState.CHOSEN:
                    if(sInput.toLowerCase().match("chosen")){
                        sReply = "Add 1 to the chosen number and give input as 'added'";
                        this.stateCur = GameState.TWICE;
                    }else{
                        sReply = "please choose a number from 1 to 20 and input as 'chosen' do not input the chosen number";
                        this.stateCur = GameState.CHOSEN;
                    }
                    break;
                    case GameState.TWICE:
                    if(sInput.toLowerCase().match("added")){
                        sReply = "Multiply 2 to the answer and input as 'multiplied'";
                        this.stateCur = GameState.FOUR;
                    }else{
                        sReply = "if you did not get, please look at the example. example: if I choose 5, add 1 to it which gives 6. then input as 'added'";
                        this.stateCur = GameState.TWICE;
                    }break;
    
                    case GameState.FOUR:
                    if(sInput.toLowerCase().match("multiplied")){
                        sReply = "Add 4 to your answer. input as 'added'";
                        this.stateCur = GameState.HALF;
                    }else{
                        sReply = "if you did not get, please look at the example. Example: if your answer was 6, multiply 6 to it. then input as 'multiplied'";
                        this.stateCur = GameState.FOUR;
                    }break;
    
                    case GameState.HALF:
                    if(sInput.toLowerCase().match("added")){
                        sReply = "divide your answer with 2 and input as  'divided'";
                        this.stateCur = GameState.PREDICTION;
                    }else{
                        sReply = "if you did not get, please look at the example. Example: if your answer was 12, add 4 and input as 'added'";
                        this.stateCur = GameState.HALF;
                    }break;
    
                    case GameState.PREDICTION:
                    if(sInput.toLowerCase().match("divided")){
                        sReply = "Subtract the answer with original chosen number and input as 'done'";
                        this.stateCur = GameState.OUTPUT;
                    }else{
                        sReply = "if you did not get, please look at the example. Example: if your answer was 16, divide 16 by 2 and input as 'divided'";
                        this.stateCur = GameState.PREDICTION;
                    }break;
    
                    case GameState.OUTPUT:
                    if(sInput.toLowerCase().match("done")){
                        sReply = "your answer is '3'. HAHA funny right!!.. if you want to continue with same game, input as 'new' or if you want to play old game, input as 'same'";
                        this.stateCur = GameState.NEW_GAME;
                    }else {
                        sReply = "if you did not get, please look at the example. Example: if your answer was 8 and initial chosen value was 5, subtract 5 from 8 and input as 'done'.";
                        this.stateCur = GameState.OUTPUT;
                    }
                    break;
            }
            return([sReply]);
    }
}